{// 常用自定义变量
    myConst = {
        appName: '',
        mainActivity: 'com.ss.android.ugc.aweme.lite/com.ss.android.ugc.aweme.main.MainActivity',
        textView: 'android.widget.TextView', // 文字控件
        viewVirw: 'android.view.View', // 视图控件
        imageView: 'android.widget.ImageView', // 图像控件
        pkg: 'com.ss.android.ugc.aweme.lite' // 程序界面控件
    };
    数据库路径 = '/sdcard/';
}
{ //常用控件信息对象
    首页 = {
        myName: '首页',
        index: 0,
        depth: 12,
        drawinGorder: 1,
        text: '首页',
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    同城 = {
        myName: '同城',
        index: 0,
        depth: 12,
        drawinGorder: 1,
        text: '同城',
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    视频介绍 = {
        myName: '视频介绍',
        index: 0,
        depth: 24,
        drawinGorder: 1,
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    来赚钱 = {
        myName: '来赚钱',
        index: 0,
        depth: 10,
        drawinGorder: 1,
        text: '',
        clz: myConst.imageView,
        pkg: myConst.pkg
    };
    限时任务领取 = {
        myName: '限时任务领取',
        index: 0,
        depth: 16,
        drawingOrder: 0,
        text: '限时任务赚金币',
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    关闭广告 = {
        myName: '关闭广告',
        index: 3,
        depth: 6,
        drawingOrder: 3,
        text: '关闭广告',
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    开宝箱 = {
        myName: '开宝箱',
        index: 1,
        depth: 13,
        drawingOrder: 0,
        text: '开宝箱得金币',
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    宝箱看广告再赚 = {
        myName: '宝箱看广告再赚',
        index: 0,
        depth: 17,
        drawingOrder: 0,
        text: '看广告视频再赚',
        clz: myConst.textView,
        pkg: myConst.pkg
    };
    签到看广告再赚 = {
        myName: '签到看广告再赚',
        index: 0,
        depth: 17,
        drawingOrder: 0,
        text: '看广告视频再赚',
        clz: myConst.viewVirw,
        pkg: myConst.pkg
    };
    签到成功 = {
        myName: '签到成功页面',
        index: 0,
        depth: 16,
        drawingOrder: 0,
        text: '签到成功',
        clz: myConst.textView,
        pkg: myConst.pkg
    };

}
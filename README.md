# EC_TikTok
请在法律许可范围内使用，如果你同步本项目则表示同意仅用于EasyClick学习目的，并同意下方的用户协议与免责声明。

本项目演示了EasyClick以下功能（个人认为有价值功能）：
1.  节点操作。
2.  根据系统时间定时循环。
3.  Sqlite的读、写操作。



#### 介绍
TikTok极速版 赚钱助手
已实现功能有：
1.  自动完成每日签到并观看广告视频。
2.  每20分钟自动领取 限时任务和开宝箱。
3.  界面滑动方式随机滑动。
4.  视频随机点赞
5.  自动跳过广告视频。


未实现的功能：
1.  APP UI设置功能。
2.  运行初始化代码（无障碍权限检测等）


#### 使用说明

1.  同步代码。
2.  Idea导入项目。
3.  打包生成APP运行即可

#### 参与贡献
期待你的出现

#### 用户协议

https://easyclick.gitee.io/docs/zh-cn/agreement.html

#### 免责声明

https://easyclick.gitee.io/docs/zh-cn/declare.html
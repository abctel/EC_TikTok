const tikTokSpeed = {};

tikTokSpeed.回到首页 = function () {
    hwx.msg('跳转到首页');
    back();
    sleep(random(500, 1500));
    hwx.click(首页);
    sleep(random(100, 3000));
};

tikTokSpeed.checkIN = function () {
    /*
     判断是否存在签到记录数据库
    */
    let tableName = "tbl_hwx";
    if (file.exists(数据库路径 + 'check_In.db') === false) {
        sqlite.connectOrCreateDb("/sdcard/check_In.db");
        hwx.msg('123');
        let columns = ["sDate", "checkIn"];
        sqlite.createTable(tableName, columns);
        let map = {
            "sDate": hwx.getDateNow(),
            "checkIn": ''
        };
        sqlite.insert(tableName, map);
        sqlite.close();
        return false;
    } else {
        /*
         签到记录查询
        */
        sqlite.connectOrCreateDb(数据库路径 + "check_In.db");
        let queryDate = hwx.getDateNow();
        let sql = 'select checkIn from tbl_hwx where sDate = \"' + queryDate + '\"';
        let data = sqlite.query(sql);
        if (data.length > 0) {
            let si = JSON.stringify(data);
            let ps = JSON.parse(si);
            sqlite.close();
            return Boolean(ps[0]["checkIn"]);
        } else {
            let map = {
                "sDate": hwx.getDateNow(),
                "checkIn": ''
            };
            sqlite.insert(tableName, map);
            sqlite.close();
            return false;
        }
    }
};

tikTokSpeed.限时任务 = function () {
    hwx.msg('开始限时任务');
    hwx.clickEX(来赚钱);
    sleep(random(2000, 5000));
    hwx.click(限时任务领取);
    sleep(40 * 1000);
    hwx.click(关闭广告);
    sleep(random(1000, 3000));
    hwx.msg('限时任务结束。');
};

tikTokSpeed.来赚钱 = function () {
    hwx.msg('开宝箱任务开始。');
    hwx.click(开宝箱);
    sleep(random(1000, 3000));
    hwx.click(宝箱看广告再赚);
    sleep(40 * 1000);
    hwx.click(关闭广告);
    sleep(random(1000, 3000));
    tikTokSpeed.回到首页();
    hwx.msg('开宝箱任务结束。');
};

tikTokSpeed.随机点赞 = function () {

    let myDeviceHeight = device.getScreenHeight() / 2; //屏幕高度的中心线
    let myDeviceWidth = device.getScreenWidth() / 2; //屏幕宽度的中心线
    let isLike = random(0, 100)
    if (isLike <= 20) {
        clickPoint(myDeviceWidth, myDeviceHeight)

        sleep(random(100, 300));
        clickPoint(myDeviceWidth, myDeviceHeight)
        hwx.msg("双击喜欢")
    }
};

modules.exports = tikTokSpeed;
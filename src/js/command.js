const hwx = {};


/**
 * 全局调试输出方法
 * <Br/>
 * 全局日志或状态信息输出。
 * <Br/>
 *
 * @param String 参数，输入需输出的字符串。
 */

hwx.msg = function (tempString) {
    logd(tempString);
    // toast(tempString);
};

/**
 * 启动APP
 * <Br/>
 * 运行环境: 无限制
 * <Br/>
 *
 * @param runSelect 参数，1为包名启动；2为应用名称启动
 * @param runString  参数，RunSelect参数为1则输入包名；为2则输入应用名称
 * @return boolean 请求后返回的字符串
 */
hwx.launch = function (runSelect, runString) {
    let isRun = false;
    // if (!autoServiceStart(3)) {
    //     logd("自动化服务启动失败，无法执行脚本");
    //     exit();
    //     return false;
    // }
    if (runSelect === 1) {
        isRun = utils.openApp(runString);
    } else if (runSelect === 2) {
        isRun = utils.openAppByName(runString);
    } else {
        hwx.msg('启动失败');
    }
    if (isRun) {
        sleep(2000);
        let ifTure = text('允许');
        let result = has(ifTure);
        if (result) {
            ifTure.click();
        }
        waitExistActivity(myConst.mainActivity,1000*10);
        hwx.msg('应用启动成功');
        return true;
    } else {
        hwx.msg('应用启动失败,脚本即将退出运行');
        exit();
        return false;
    }
};

/**
 * Text对象点击控件
 * <Br/>
 * 通过Text点击对象点击按钮。
 * <Br/>
 *
 * @param tempString 参数：Text的点击控件的对象信息。
 * @return boolean 返回执行结果
 */
hwx.click = function (tempString) {
    do {
        lockNode();
        let node = text(tempString.text).getOneNodeInfo(0);
        releaseNode();
        if (node) {
            if (node.click()) {
                hwx.msg('点击' + tempString.myName + '成功');
                break;
            } else {
                if (node.clickEx()) {
                    hwx.msg('点击EX' + tempString.myName + '成功');
                    break;
                } else {
                    hwx.msg('点击EX' + tempString.myName + '失败');
                }
            }
        }
        sleep(1000);
    } while (true)
};

/**
 * 获取当天日期
 * <Br/>
 * 获取的格式为 YYYY-MM-DD
 * <Br/>
 *
 * @return string 返回"YYYY-MM-DD"格式字符串
 */
hwx.getDateNow = function () {
    let sdate = new Date();
    let year = sdate.getFullYear();
    let month = sdate.getMonth() + 1;
    let day = sdate.getDate();

    return year + "-" + month + "-" + day;
}

/**
 * 自定义对象点击控件
 * <Br/>
 * 通过自定义点击对象点击按钮。
 * <Br/>
 *
 * @param String 参数：自定义的点击控件的对象信息。
 * @return boolean 返回执行结果
 */
hwx.clickEX = function (tempString) {
    // let result = false;

    do {
        lockNode();
        let node = index(tempString.index)
            .depth(tempString.depth)
            .drawingOrder(tempString.drawingOrder)
            .text(tempString.text)
            .clz(tempString.clz)
            .pkg(tempString.pkg);
        // logd(tempString.index);
        // logd(tempString.depth);
        // logd(tempString.drawingOrder);
        // logd(tempString.text);
        // logd(tempString.clz);
        // logd(tempString.pkg);
        releaseNode();
        if (node) {
            if (click(node)) {
                hwx.msg('点击' + tempString.myName + '成功');
                break;
            } else {
                if (clickEx(node)) {
                    hwx.msg('点击EX' + tempString.myName + '成功');
                    break;
                } else {
                    hwx.msg('点击EX' + tempString.myName + '失败');
                }
            }
        }
        sleep(1000);
    } while (true);
};

/**
 * 广告视频检测
 * <Br/>
 * 检测当前视频是否是广告视频。
 * <Br/>
 *
 // * @param tempString 参数：自定义的点击控件的对象信息。
 * @return Integer 请求后返回广告标志位于多少位（即大于0）
 */

hwx.adverstising = function () {
    lockNode();
    let selector = index(视频介绍.index)
        .depth(视频介绍.depth)
        .drawingOrder(视频介绍.drawingOrder)
        .clz(视频介绍.clz)
        .pkg(视频介绍.pkg);
    releaseNode();
    let result = getText(selector);
    let tempString = result.toString();
    let inroads = tempString.search(/[t]/i);
    return inroads > -1;
};

/**
 * 视频滑动
 * <Br/>
 * 视频随机滑动。
 * <Br/>
 *
 * @return Boolean 返回滑动运行结果。
 */
hwx.swipeParabola = function () {
    let myDeviceHeight = device.getScreenHeight() / 2; //屏幕高度的中心线
    let myDeviceWidth = device.getScreenWidth() / 2; //屏幕宽度的中心线
    let mySwipeDistance = myDeviceHeight - 100; //屏幕滑动距离
    let offset = random(0, 100); // 屏幕高度的偏移距离
    let sX = myDeviceWidth - random(0, 100); //随机开始的x坐标
    let sY = myDeviceHeight + offset + (mySwipeDistance / 2); // 随机开始的y坐标
    let eX = myDeviceWidth + random(-50, 50); //随机结束的x坐标
    let eY = myDeviceHeight - offset - (mySwipeDistance / 2); //随机结束的y坐标
    let tT = random(300, 800); // 随机滑动时间
    let isBol = swipeToPoint(sX, sY, eX, eY, tT);
    if (isBol) {
        hwx.msg('滑动成功');
    } else {
        hwx.msg('滑动失败');
    }
    return isBol;
};

function autoServiceStart(time) {
    for (let i = 0; i < time; i++) {
        if (isServiceOk()) {
            return true;
        }
        let started = startEnv();
        logd("第" + (i + 1) + "次启动服务结果: " + started);
        if (isServiceOk()) {
            return true;
        }
    }
    return isServiceOk();
}

modules.exports = hwx;